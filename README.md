# MM678-Merge-Testing

Test suite for MM678-merge (see https://gitlab.com/templayer/mmmerge)

## How does it work?

All you have to do is to launch the test suite, leave the computer alone for a while, and take a look at the test report later. The testing framework will run the game and test the game behavior in various scenarios.

The tests control the game using several tools:
- [Python Unittest](https://docs.python.org/3/library/unittest.html). Though, the game is tested as a blackbox.
- [PyWinAuto](https://pywinauto.readthedocs.io/en/latest/) runs the game and imitates pointing and clicking the mouse, pressing the keys, etc.
- [MMExt-based LUA debug scripts](https://grayface.github.io/mm/ext/ref/) are used to give some commands and to analyze the game state.
- Other used packages include [Python Configparser](https://docs.python.org/3/library/configparser.html), html-testRunner, shutil **TODO** etc.
- **TODO** PIL and PyTesseract, if we need to analyze the graphics part.

In short, the test scripts do the following:
- **Launch the game.** Usually (**TODO** planned) it is done only once for many tests. The game needs to be re-launched only if you need different mm8.ini (or other config) parameters or if one of the tests lead to a crash.
- **Load some pre-defined game state.** Mainly, pre-made save file is copied into one of the save slots, and then the game from that slot is loaded using point-and-clicks imitated by PyWinAuto.
- **Click around** (as if it was done by the player). Open the chest, cast the spell, enter the house, interact with NPC, etc. PyWinAuto handles that by imitating point-and-clicks. As if they were coming from the user.
- **Verify the outcome** Check if the actions lead to the expected outcomes. There is no direct access to the game internal state, so this step is a bit tricky.
    - PyWinAuto presses the keys to call the game LUA debug console, and then type in debug commands.
    - LUA debug command dumps essential game state into the text file
    - Unittest test case parses the file and checks if the game state matches the expectation.
- **Write the report**. Did the test pass or fail? If fail, for what reason?

**TODO** see detailed description of a few tests. Make the descriptions more detailed and move them. Leave just the link here.

A simple test suite can look like this: **TODO** work on it after sample scenario
- Launch the game.
- Load the game where the character has various scrolls in the inventory. Imitate mouse clicks on Load Game.
- Each test will pick one scroll, cast it and make sure that the casting result matches expectations.
- Game is relaunched only if previous test lead to a crash. Game is reloaded only if previous test made current game state unusable (e.g. looted the monster,
while next test attempts to reanimate it). **TODO** how to do it? Should be possible through debug console or just right clicks.


A more complex test scenario can look like this:
- Launch the game.
- Load the game where the party is in front of a place of power.
- Do additional actions to activate that place of power.
- Cast Town Portal. Imitate mouse clicks: Cast spell -> Water Magic -> Town Portal. The real spell in that case should be dimension door, not town portal.
- Click on Antagarich and wait for teleportation to complete.
- Make sure the party is really in Atnagarich after that. Check internal variables using debug console.

## Quick Start Guide

- Install Python 3 and PyWinAuto package.
- Unpack/clone the test kit.
- Create the file test.ini. It contains installation-dependent configuration parameters. You can copy test_template.ini and edit it accordingly.
- Open command prompt. If the game requires administrator privilleges, you have to run CMD with administrator privilleges too.
- Run all the test cases in current directory:
``
python run_all_tests.py
``
It will also generate HTML report in ./Report. Or for particular test case:
``
python -m unittest Tests\Sample\test_dd_to_antag.py
``
- Leave the computer alone for the time of the testing. All clicking and key pressing will be done for you
- Read the test report


## <More advanced start guide>

**TODO**

## Reading the Reports

**TODO**

## Precautions

**DO NOT INTERFERE WHILE THE TESTS ARE RUNNING**

The tests overwrite autosaves, quicksaves and mm8.ini. .

## Notes
