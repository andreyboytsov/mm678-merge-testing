# Sample Tests

This directory contains example test cases. Detailed description follows.

## test_dd - Dimension Door spell tests

Each test works as follows:
- The party is at a place of power
- The party casts Town Portal. Dimension Door should be cast instead.
- New continent is selected.
- The test checks that the party ended up on a new continent