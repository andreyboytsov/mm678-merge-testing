# It is a very quick-n-dirty sketch, just to show general direction of thought.
import shutil
import logging
import test_utils
import unittest
import os
import time

# TODO: between tests check if something went wrong
# Return situation to normal if needs be. 

class SampleDimensionDoorMergeTest(test_utils.TestCaseTemplate):
    """
    TODO
    """
    @classmethod
    def setUpClass(cls):
        cls.merge = test_utils.MergeApp()
        cls.merge.start_app()
        cls.merge.bind_key_to_command("F1",
                test_utils.generate_debug_dump_command("Map.Name","test.txt"))
    
    def activate_ns_place_of_power(self):
        """
        Activates the place of power in New Sorpigal.
        """
        logging.info("Drinking from the fountain 50 times")
        for i in range(50):
            SampleDimensionDoorMergeTest.merge.interact()
            logging.info("Drank from the fountain %d times", i+1)
    
    def setUp(self):
        super().setUp()
        # It will be run before each test
        # We need to make sure the app is ready:
        #  - Is application running?
        #  - Is application in the main screen?
        #  - Is debug window closed?
        if not SampleDimensionDoorMergeTest.merge.is_running():
            SampleDimensionDoorMergeTest.merge.start_app()
        SampleDimensionDoorMergeTest.merge.to_main_menu()
    
    def cast_and_test_dimension_door(self, continent, activate_function):
        """
        Casts dimension door to a certain continent. Tests that the party
        really ended up on that continent
        """
        if continent==test_utils.CONTINENT_ENROTH:
            coord_x = test_utils.dd_enroth_relative_x
            coord_y = test_utils.dd_enroth_relative_y
        elif continent==test_utils.CONTINENT_ANTAGARICH:
            coord_x = test_utils.dd_antagarich_relative_x
            coord_y = test_utils.dd_antagarich_relative_y
        else:
            coord_x = test_utils.dd_jadame_relative_x
            coord_y = test_utils.dd_jadame_relative_y
        
        activate_function()
        
        SampleDimensionDoorMergeTest.merge.cast(test_utils.SPELL_TOWN_PORTAL)
        SampleDimensionDoorMergeTest.merge.click(coord_x, coord_y)
        logging.info("Selected %s", continent)
        SampleDimensionDoorMergeTest.merge.click(coord_x, coord_y)
        logging.info("Cast dimension door to %s", continent)
        SampleDimensionDoorMergeTest.merge.wait_travel()
        SampleDimensionDoorMergeTest.merge.press_keys("{F1}")
        time.sleep(2)

        with open(os.getcwd()+"\\Temp\\test.txt","rt") as f:
            map_name = f.readline()
            logging.info("Map name: %s", map_name)
            text_map_name = test_utils.get_plain_text_map_name(map_name)
            logging.info("Map name: %s. Plain text map name: %s",
                         map_name, text_map_name)
            new_continent = test_utils.get_continent(text_map_name)
        logging.info("New continent: %s. Expected continent: %s",
                     new_continent, continent)
        self.assertEqual(new_continent, continent)
    
    def load_ns_place_of_power(self):
        """
        Loads a save: party is at a place of power in New Sorpigal
        """
        shutil.copyfile("./TestSaves/sample_test_atNSfountain.dod",
                        test_utils.get_game_folder()+"/Saves/autosave.dod")
        logging.info("Copied the test file: ./TestSaves/test001_atNSfountain.dod -> ./Saves/autosave.dod")
        SampleDimensionDoorMergeTest.merge.load_autosave()
    
    def test_dd_enroth_to_antagarich_ns(self):
        """
        Test dimension door casting from a place of power in New Sorpigal to Antagarich
        """
        self.load_ns_place_of_power()
        self.cast_and_test_dimension_door(
                    continent=test_utils.CONTINENT_ANTAGARICH,
                    activate_function=self.activate_ns_place_of_power)

    def test_dd_enroth_to_jadame_ns(self):
        """
        Test dimension door casting from a place of power in New Sorpigal to Antagarich
        """
        self.load_ns_place_of_power()
        self.cast_and_test_dimension_door(
                    continent=test_utils.CONTINENT_JADAME,
                    activate_function=self.activate_ns_place_of_power)

    def test_dd_enroth_to_enroth_ns(self):
        """
        Test dimension door casting from a place of power in New Sorpigal to Antagarich
        """
        self.load_ns_place_of_power()
        self.cast_and_test_dimension_door(
                    continent=test_utils.CONTINENT_ENROTH,
                    activate_function=self.activate_ns_place_of_power)

    @classmethod
    def tearDownClass(cls):
        cls.merge.stop_app()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    unittest.main()