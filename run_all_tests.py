import unittest
import logging
import HtmlTestRunner

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    
    suite = unittest.TestLoader().discover('.', pattern = "test_*.py")
    
    runner = HtmlTestRunner.HTMLTestRunner(output='Report')
    runner.run(suite)