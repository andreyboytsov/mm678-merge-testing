import logging
import pywinauto
import configparser
import time
import os
import unittest

# Copied from /Data/Tables/Outdoor travels.txt
# It seems, Jadame is default, and it might ne related to the fact that the
# merge is based on MM8 engine.

CONTINENT_ANTAGARICH = 'Antagarich'
CONTINENT_ENROTH = 'Enroth'
CONTINENT_JADAME = 'Jadame'

MAP_EVENMORN = 'Evenmorn Island'
MAP_AVLEE = 'Avlee'
MAP_HARMONDALE = 'Harmondale'
MAP_ERATHIA = 'Erathia'
MAP_TULAREAN_FOREST = 'The Tularean Forest'
MAP_DEYJA = 'Deja' # That was in game scripts
MAP_BRACADA = 'The Bracada Desert'
MAP_BARROW_DOWNS = 'The Barrow Downs'
MAP_TATALIA = 'Tatalia'
MAP_SHOALS = 'Shoals'
MAP_SWEET_WATER = 'Sweet Water'
MAP_PARADISE_VALLEY = 'Paradise Valley'
MAP_HERMITS_ISLE = "Hermit's Isle"
MAP_KRIEGSPIRE = 'Kriegspire'
MAP_BLACKSHIRE = 'Blackshire'
MAP_DRAGONSAND = 'Dragonsand'
MAP_FROZEN_HIGHLANDS = 'Frozen Highlands'
MAP_FREE_HAVEN = 'Free Haven'
MAP_MIRE_OF_THE_DAMNED = 'Mire of the Damned'
MAP_SILVER_COVE = 'Silver Cove'
MAP_RAVENSHORE = 'Ravenshore'
MAP_SHADOWSPIRE = 'Shadowspire'
MAP_ALVAR = 'Alvar'
MAP_DAGGER_WOUND_ISLAND = 'Dagger Wound Island'
MAP_GARROTE_GORGE = 'Garrote Gorge'
MAP_REGNA = 'Regna'
MAP_BOOTLEG_BAY = 'Bootleg Bay'
MAP_CASTLE_IRONFIST = 'Castle Ironfist'
MAP_EEL_INFESTED_WATERS = 'Eel Infested Waters'
MAP_MISTY_ISLANDS = 'Misty Islands'
MAP_NEW_SORPIGAL = 'New Sorpigal'
MAP_MURMURWOODS = 'Murmurwoods'
MAP_IRONSAND = 'Ironsand Desert'
MAP_RAVAGE_ROAMING = 'Ravage Roaming'

map_file_to_name = {
  "out09.odm": MAP_EVENMORN,
  "7out02.odm": MAP_HARMONDALE,
  "7out03.odm": MAP_ERATHIA,
  "7out04.odm": MAP_TULAREAN_FOREST,
  "7out05.odm": MAP_DEYJA,
  "7out06.odm": MAP_BRACADA,
   "out11.odm": MAP_BARROW_DOWNS,
  "7out13.odm": MAP_TATALIA,
  "out14.odm": MAP_AVLEE,
  "outa1.odm": MAP_SWEET_WATER,
  "outa2.odm": MAP_PARADISE_VALLEY,
  "outa3.odm": MAP_HERMITS_ISLE,
  "outb1.odm": MAP_KRIEGSPIRE,
  "outb2.odm": MAP_BLACKSHIRE,
  "outb3.odm": MAP_DRAGONSAND,
  "outc1.odm": MAP_FROZEN_HIGHLANDS,
  "outc2.odm": MAP_FREE_HAVEN,
  "outc3.odm": MAP_MIRE_OF_THE_DAMNED,
  "outd1.odm": MAP_SILVER_COVE,
  "outd2.odm": MAP_BOOTLEG_BAY,
  "outd3.odm": MAP_CASTLE_IRONFIST,
  "oute1.odm": MAP_EEL_INFESTED_WATERS,
  "oute2.odm": MAP_MISTY_ISLANDS,
  "oute3.odm": MAP_NEW_SORPIGAL,
  "7out15.odm": MAP_SHOALS,
  "out07.odm": MAP_MURMURWOODS,
  "out01.odm": MAP_DAGGER_WOUND_ISLAND,
  "out02.odm": MAP_RAVENSHORE,
  "out03.odm": MAP_ALVAR,
  "out04.odm": MAP_IRONSAND,
  "out05.odm": MAP_GARROTE_GORGE,
  "out06.odm": MAP_SHADOWSPIRE,
  "out13.odm": MAP_REGNA,
}

# TODO: Rework it, add Jadame explicitly and don't make it default
# Otherwise any weird map will be counted as Jadame
map_to_continent = {
    MAP_HARMONDALE: CONTINENT_ANTAGARICH,
    MAP_ERATHIA: CONTINENT_ANTAGARICH,
    MAP_TULAREAN_FOREST: CONTINENT_ANTAGARICH,
    MAP_DEYJA: CONTINENT_ANTAGARICH,
    MAP_BRACADA: CONTINENT_ANTAGARICH,
    MAP_BARROW_DOWNS: CONTINENT_ANTAGARICH,
    MAP_TATALIA: CONTINENT_ANTAGARICH,
    MAP_EVENMORN: CONTINENT_ANTAGARICH, 
    MAP_AVLEE: CONTINENT_ANTAGARICH,
    MAP_SWEET_WATER: CONTINENT_ENROTH,
    MAP_PARADISE_VALLEY: CONTINENT_ENROTH,
    MAP_HERMITS_ISLE: CONTINENT_ENROTH,
    MAP_KRIEGSPIRE: CONTINENT_ENROTH,
    MAP_BLACKSHIRE: CONTINENT_ENROTH,
    MAP_DRAGONSAND: CONTINENT_ENROTH,
    MAP_FROZEN_HIGHLANDS: CONTINENT_ENROTH,
    MAP_FREE_HAVEN: CONTINENT_ENROTH,
    MAP_MIRE_OF_THE_DAMNED: CONTINENT_ENROTH,
    MAP_SILVER_COVE: CONTINENT_ENROTH,
    MAP_BOOTLEG_BAY: CONTINENT_ENROTH,
    MAP_CASTLE_IRONFIST: CONTINENT_ENROTH,
    MAP_EEL_INFESTED_WATERS: CONTINENT_ENROTH,
    MAP_MISTY_ISLANDS: CONTINENT_ENROTH,
    MAP_NEW_SORPIGAL: CONTINENT_ENROTH,
    MAP_SHOALS: CONTINENT_ANTAGARICH,
    MAP_MURMURWOODS: CONTINENT_JADAME,
    MAP_IRONSAND: CONTINENT_JADAME,
    MAP_RAVAGE_ROAMING: CONTINENT_JADAME,
    MAP_ALVAR: CONTINENT_JADAME,
    MAP_DAGGER_WOUND_ISLAND: CONTINENT_JADAME,
    MAP_RAVENSHORE: CONTINENT_JADAME,
    MAP_GARROTE_GORGE: CONTINENT_JADAME,
    MAP_SHADOWSPIRE: CONTINENT_JADAME,
    MAP_REGNA: CONTINENT_JADAME,
}

default_width = 640
default_height = 480

new_game_relative_x = 0.89
new_game_relative_y = 0.4688
load_game_relative_x = 0.89
load_game_relative_y = 0.5625
up_button_relative_x = 0.4570
up_button_relative_y = 0.4771
first_save_relative_x = 0.1602
first_save_relative_y = 0.4771
water_magic_relative_x = 0.9648
water_magic_relative_y = 0.2344
town_portal_relative_x = 0.1719
town_portal_relative_y = 0.8646

dd_antagarich_relative_x = 0.9273
dd_antagarich_relative_y = 0.2396
dd_enroth_relative_x = 0.4921
dd_enroth_relative_y = 0.0583
dd_jadame_relative_x = 0.0594
dd_jadame_relative_y = 0.2083

quit_game_relative_x = 0.7188
quit_game_relative_y = 0.6458


def generate_debug_dump_command(output, temp_filename = "test.txt"):
    return 'file = io.open{(}"'+\
        os.getcwd().replace('\\','\\\\') +\
        '\\\\Temp\\\\'+temp_filename+'", "w"{)}{ENTER}'+\
        'file:write{(}'+output+'{)}{ENTER}'+\
        'file:close{(}{)}'


def get_game_folder():
    test_ini = configparser.ConfigParser()
    test_ini.read("test.ini")
    return test_ini["Settings"]["GameFolder"]


def int_value_or_minus_one(mm8_ini, key):
    """
    TODO
    """
    try:
        if key not in mm8_ini['Settings']:
            return -1
        return int(mm8_ini['Settings'][key])
    except ValueError:
        return -1


def get_width_and_height(mm8_ini):
    """
    TODO
    """
    width = int_value_or_minus_one(mm8_ini, 'WindowWidth')
    height = int_value_or_minus_one(mm8_ini, 'WindowHeight')

    if width == -1 and height == -1:
        return default_width, default_height
    elif width == -1:
        return (int(default_width * height / default_height),
                height)
    elif height == -1:
        return (width,
                int(default_height * width / default_width))
    else:
        return width, height


def get_continent(plain_text_map_name):
    """
    TODO
    Works only outdoors ATM
    """
    # TODO: should also work indoors?
    return map_to_continent[plain_text_map_name]


def get_plain_text_map_name(map_name):
    """
    @param map_name Filename of the map
    @returns Continent name for the map
    """
    return map_file_to_name[map_name]


SCREEN_MAIN_MENU = "MainMenu"
SCREEN_ESC = "EscapePressedFromGame"
SCREEN_LOAD = "LoadGameScreen"
SCREEN_SAVE = "SaveGameScreen"

SPELL_TOWN_PORTAL = "Town Portal"

MAGIC_WATER = "Water Magic"

# TODO fill in
magic_by_spell = {
    SPELL_TOWN_PORTAL: MAGIC_WATER,
}

# TODO fill in
coords_by_magic = {
    MAGIC_WATER: (water_magic_relative_x, water_magic_relative_y),
}

# TODO fill in
coords_by_spell = {
    SPELL_TOWN_PORTAL: (town_portal_relative_x, town_portal_relative_y),
}


# TODO Due to issue #330 I have to run this script from current directory
# TODO Need to read the keyboard settings and use it

class MergeApp:
    """
    TODO
    """

    def __init__(self, path_to_merge = get_game_folder()):
        self.path_to_merge = path_to_merge
        if not self.path_to_merge.endswith("/") and \
                not self.path_to_merge.endswith("\\"):
            self.path_to_merge += "/"
        logging.info("Initialized MergeApp: %s", path_to_merge)
        self.mm8_ini = None
        self.merge_app = None
        self.merge_window = None
        self.debug_window = None
        self.width = None
        self.height = None
        self.save_number_limit = None

    def stop_app(self):
        self.merge_app.kill()
        self.mm8_ini = None
        self.merge_app = None
        self.merge_window = None
        self.debug_window = None
        self.width = None
        self.height = None
        self.save_number_limit = None

    # TODO: parametrize - switching screens from what to what?
    def wait_screen_switch(self, prev_screen=None, next_screen=None):
        """
        TODO
        
        None - unknown or should not matter
        """
        time.sleep(3)  # Preferably, detect when the screen is switched. But that will do for now.

    def wait_game_load(self):
        """
        TODO
        """
        time.sleep(10)  # Loading the save can take a bit. Again, we'd better find a way to just make sure it is loaded.

    def wait_travel(self):
        """
        TODO
        """
        time.sleep(5)  # Preferably, detect when the trave is complete. But that will do for now.    

    def start_app(self):
        """
        Launches MM678-merge and leaves it at the main screen.
        
        Beforhand it also reads all configuration files and fills in
        configuration variables with config files information at the time of
        the merge.
        """
        self.mm8_ini = configparser.ConfigParser()
        self.mm8_ini.read(self.path_to_merge + 'mm8.ini')
        self.width, self.height = get_width_and_height(self.mm8_ini)
        logging.info("Screen width and height: %d %d", self.width, self.height)
        self.save_number_limit = 20  # TODO: read from config, when implemented

        # TODO What if the keys are changed? Read keyboard config, not hardcode it
        self.key_interact = "{SPACE}"
        self.key_cast = "c"

        cur_dir = os.getcwd()
        os.chdir(self.path_to_merge)
        logging.info("Changed the directory to path to merge: %s", self.path_to_merge)
        self.merge_app = pywinauto.Application(backend="win32").start("mm8.exe")
        logging.info("Launched the merge")
        os.chdir(cur_dir)
        logging.info("Changed the directory back: %s", cur_dir)
        time.sleep(3) # Sometimes we get delayed and start clicking around prematurely

        self.merge_window = self.merge_app.window(title="Might and Magic® VIII")
        r = self.merge_window.rectangle()
        logging.info(
            "Window coordinates: left %d, right %d , top %d, bottom %d",
            r.left, r.right, r.top, r.bottom)
        # TODO: CAREFUL! Not all difference is in top padding or left padding. Calculate properly.
        self.left_padding = (r.width() - self.width) // 2
        self.top_padding = (r.height() - self.height) // 2
        logging.info("Bars: left %d, top %d", self.left_padding, self.top_padding)
        # TODO Don't do it? Or at least have a parameter to not do it.
        self.merge_window.click_input()  # Clicking anywhere just to get the main screen
        logging.info("Clicked somewhere to go to the main screen")
        self.wait_screen_switch(prev_screen=None, next_screen=SCREEN_MAIN_MENU)

    def is_running(self):
        if self.merge_window is None:
            return False
        return self.merge_app.is_process_running()


    # TODO This method needs to be way more robust. At the moment it assumes
    # that we are in a game, and we are not waiting for input. But it should
    # work in 
    def to_main_menu(self):
        """
        TODO
        """
        logging.info("Returning to the main menu")
        self.merge_window.type_keys("{ESC}")
        self.click(quit_game_relative_x, quit_game_relative_y)
        self.click(quit_game_relative_x, quit_game_relative_y)
        logging.info("Should be at the main menu now")

    def open_debug_window(self):
        """
        Opens debug window. It pauses the game and obscures the window (
        important for clicking). Therefore, debug window should be closed after
        use. 
        """
        self.merge_window.type_keys("^{F1}")
        logging.info("Opened debug window")
        self.debug_window = self.merge_app.window(title="MMExtension Debug Console")
        
    def close_debug_window(self):
        """
        Closes debug window if it is open. Otherwise does nothing.
        """
        if self.debug_window is not None:
            self.debug_window.close()
        self.debug_window = None

    def click(self, x, y, button="left"):
        """
        TODO
        
        x,y are [0,1] relative to screen width and height.
        """
        click_x = int(self.left_padding + x * self.width)
        click_y = int(self.top_padding + y * self.height)
        logging.info("Clicking %d %d", click_x, click_y)
        self.merge_window.click_input(button=button,
                                      coords=(click_x, click_y))

    # TODO split it in several methods later
    def load_autosave(self, prev_screen=SCREEN_MAIN_MENU):
        """
        TODO
        """
        # TODO what if loading from inside the game? Need other coordinates

        # Press "Load game"
        self.click(load_game_relative_x, load_game_relative_y)
        logging.info("Clicked LOAD GAME")
        self.wait_screen_switch(prev_screen, SCREEN_LOAD)

        # TODO skipping it for now just to make it faster
        # Let's click up button 20 times. With current save limit it ensures that we are at the very top.
        # Tough, for real testing we'd better make a requirement that Save folder is clean
        # for i in range(self.save_number_limit):
        #    self.click(up_button_relative_x,up_button_relative_y)
        #    logging.info("Pressed UP %d times",i+1)

        # Load the first save
        self.click(first_save_relative_x, first_save_relative_y)
        logging.info("Selected autosave")
        self.click(first_save_relative_x, first_save_relative_y)
        logging.info("Loading autosave")
        self.wait_game_load()

    def interact(self):
        """
        TODO
        """
        self.merge_window.type_keys(self.key_interact)  # TODO What if key is changed?
        
    def press_keys(self, keys):
        """
        TODO
        """
        self.merge_window.type_keys(keys)  # TODO What if key is changed?

    # TODO There should be 2 casting methods: by clicking, or by debug console
    # TODO Customize (e.g. cast spell by pressing "C" or by clicking?)
    # TODO set decent defaults
    def cast(self, spell_name):
        """
        TODO 
        
        Casts the spell. However, additional clicks should be done manually.
        """
        self.merge_window.type_keys(self.key_cast)
        logging.info("Entered casting mode")
        magic = magic_by_spell[spell_name]

        self.click(coords_by_magic[magic][0], coords_by_magic[magic][1])
        logging.info("Selected %s", magic)

        self.click(coords_by_spell[spell_name][0], coords_by_spell[spell_name][1])
        logging.info("Selected %s", spell_name)
        self.click(coords_by_spell[spell_name][0], coords_by_spell[spell_name][1])
        logging.info("Cast %s", spell_name)
    
    
    def bind_key_to_command(self, key, command):
        """
        Assigns LUA function to a certain key.
        """
        if self.debug_window is None:
            self.open_debug_window()
        entire_command = "function{SPACE}events.KeyDown{(}t{)}{ENTER}"+\
                        "if{SPACE}t.Key==const.Keys."+key+"{SPACE}then{ENTER}"+\
                        command+"{ENTER}end{ENTER}end^{ENTER}"
        self.debug_window.type_keys(entire_command)
        self.close_debug_window()
            
    
    def write_to_temp_file(self, output, temp_filename = "test.txt"):
        """
        TODO
        """
        if self.debug_window is None:
            self.open_debug_window()
        debug_command = generate_debug_dump_command(output,
                                                    temp_filename) + '^{ENTER}'
        self.debug_window.type_keys(debug_command)
                    
        
class TestCaseTemplate(unittest.TestCase):
    
    def setUp(self):
        if not os.path.isdir("./Temp"):
            os.mkdir("./Temp")
        os.system("echo y | del Temp\\*")
    
